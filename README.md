# NASA Astronomy Picture of the Day Fetcher
Sets the NASA Astronomy POTD as wallpaper for Gnome 3 (MATE)

### Requirements
* Python3
* Beautiful Soup (bs4)
* Pillow (PIL)

### Usage
* python potd_fetcher.py
* If you want to check for images automatically, put the script in your crontab.

### Notes

Tested under Ubuntu 18.10
